package com.hyh.validator.web;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 * @author Summerday
 */

@RestController
@Validated
public class OnlyParamsController {

    @GetMapping("/{id}/{name}")
    public String test(@PathVariable("id") @Min(1) Long id,
                       @PathVariable("name") @Size(min = 5, max = 10) String name) {
        return "success";
    }
}
