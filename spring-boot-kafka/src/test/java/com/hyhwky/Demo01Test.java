package com.hyhwky;

import com.hyhwky.producer.Demo01Producer;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.support.SendResult;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

/**
 * @author Summerday
 */

@SpringBootTest
@Slf4j
public class Demo01Test {

    @Autowired
    Demo01Producer producer;

    @Test
    void testSyncSend() throws ExecutionException, InterruptedException {

        long id = System.currentTimeMillis();
        SendResult<Object, Object> result
                = producer.syncSend((int) id);
        log.info("发送编号: {}, 发送结果 : {}", id, result);
        new CountDownLatch(1).await();
    }
}
