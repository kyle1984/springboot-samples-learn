package com.hyh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSlimmingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSlimmingApplication.class, args);
    }

}
