package com.hyh.h2.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hyh.h2.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Summerday
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
