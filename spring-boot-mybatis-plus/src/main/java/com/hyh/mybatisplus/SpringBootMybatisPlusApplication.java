package com.hyh.mybatisplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMybatisPlusApplication {

    public static void main(String[] args) {
        //System.out.println(System.getProperty("user.dir") + "\\spring-boot-mybatis-plus\\");
        SpringApplication.run(SpringBootMybatisPlusApplication.class, args);
    }

}
