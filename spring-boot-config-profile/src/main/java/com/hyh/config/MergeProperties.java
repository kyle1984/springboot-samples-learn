package com.hyh.config;

import com.hyh.entity.User;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.support.SimpleTriggerContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Summerday
 */
@ToString
@ConfigurationProperties(prefix = "merge")
public class MergeProperties {


    private final List<User> list = new ArrayList<>();

    private final Map<String, User> map = new HashMap<>();

    public List<User> getList() {
        return list;
    }

    public Map<String, User> getMap() {
        return map;
    }
}
